const { Keyboard } = require('telegram-keyboard');
const keyboard_list = require('./keyboard')

module.exports = new class dev_bot {
    // keyboard making
    key_make(keyboard_name){
        return Keyboard.make(keyboard_list[keyboard_name])
    }
}