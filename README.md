# Jtelb

**Jtelb**  is a special telegram bot framework based on telegrafjs that makes some stuff auto and close to web Node js project structure.

## Installation
```
npm install jtelb
```

## Usage
**require jtelb**
```
const { bot } = require('jtelb')
bot.config_bot('bot token', 'welcome message')
```


**if you are using nodemon**

**ignore local session file**

*jtelb_data.json should ignore if you are using nodemon*

add  --ignore jtelb_data.json  for start command like this:
```
"start": "nodemon index.js --ignore jtelb_data.json",
```


*start bot*
**start bot to generate directories for you => npm start => folders will generate automaticly for you**


**/bot_app**

* all the folders and files will generate inside /bot_app and your bot application is here 
it's because that you can add telegram bot to your nodejs web application



**/functions**

* you can define your functions and functions directory so you can use them in replies
```
module.exports = new class sample_function {
    sample(ctx) {
        ctx.reply('this is functions answer!!')
    }
    
    answer_me(){
        ctx.reply('this is your answer')
    }
}
```


**/keyboard**

* you can define your both simple keyboards and inline keyboards in a same way in  this file like this sample
* => after define you have to export that
```
main_keyboard = [
    ['Hello world!'],
    ['ُSend several answers'],
    ['Change keyboard', 'Inline keyboard'],
    ['Answer by function'],
    ['About Jtelb'],
]
```
**get keyboards list in your development**
```
const { keyboard_list } = require('jtelb');
```

**keyboard make function**
```
const { key_make } = require('jtelb')

function sample_function(ctx, next) {
    let keyboard = key_make('keyboard name')
    ctx.reply('change keyboard for example', keyboard.reply())
    next();
}
```

**/middleware**

* you can define your middlewares here like this:
```
function sample_middleware(ctx, next) {   
    ctx.reply('middleware is working! ' + ctx.from.username) 
    next()
}

module.exports = {
    sample_middleware,
}
```
* you should define function and export it


**/reply**

* in reply directory you can define answers, a way of sending answers, sessions conditions and session that you want define after answer
* look at this following example:
```
module.exports.main_keyboard_replyes = {
    "intered text": [ 
        [
          'session',
          "answer", /* you can put fun. before that and call your function */
          'next keyboard name',  /* you can put inline. before that to make it inline keyboard */
          'next session' /* it can be anything you like */
        ] 
    ],
}
```


** ask us your questions in telegram group: **

https://t.me/jtelb
```
@jtelb
```


### Github repository
https://github.com/onlineabedini/jtelb/

### npm
https://www.npmjs.com/package/jtelb
